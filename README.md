# xhcdApp


ASPNet MVC Core 3.1  Aplicacion de ejemplo consumo de API externa  

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

```
- git clone $(url del repo)
-abrir con visualStudio 2019 o visual studio Code 

```


### Pre-requisitos 📋

_Que cosas necesitas para instalar el software y como instalarlas_

```
-Visual Studio 2019
-SDK aspnet core 3.1

```

### Instalación 🔧

_Una serie de ejemplos paso a paso que te dice lo que debes ejecutar para tener un entorno de desarrollo ejecutandose_

```
- clonar solucion a tu equipo local 
-compilar 
-ejecutar
```


## Ejecutando las pruebas ⚙️

*TODO

### Analice las pruebas end-to-end 🔩


## Despliegue 📦
la Api esta alojada en los servicios de Azure , al momento de publicar se encuentra activo el servicio ... notificar en caso de fallo .

se puede testear con Postamt

_url: https://xhcdapi.azurewebsites.net/comic

https://xhcdapi.azurewebsites.net/comic/1

## Construido con 🛠️

_Menciona las herramientas que utilizaste para crear tu proyecto_

* [AspNetCore 3.1](https://dotnet.microsoft.com/download) - framework NetCore 3.1
* [NewtonSoft](https://www.newtonsoft.com/json) - paquede necesario para trabajar con Json
* [VisualStudio 2019](hhttps://visualstudio.microsoft.com/es/vs/) - IDE

## Versionado 📌

Usamos [GitHub](https://github.com/) para el versionado.


## Autores ✒️

_Menciona a todos aquellos que ayudaron a levantar el proyecto desde sus inicios_

* **Alexander J. Cano** - *Trabajo Inicial* - [AJcano09](https://github.com/AJcano09)


## Licencia 📄

Este proyecto está bajo la Licencia (Tu Licencia) - mira el archivo [LICENSE.md](LICENSE.md) para detalles

## Expresiones de Gratitud 🎁

* Comenta a otros sobre este proyecto 📢
* Invita una cerveza 🍺 o un café ☕ a alguien del equipo. 
* Da las gracias públicamente 🤓.
* etc.

