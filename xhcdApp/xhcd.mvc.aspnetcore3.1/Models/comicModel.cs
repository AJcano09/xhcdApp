﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace xhcd.mvc.aspnetcore3._1.Models
{
    public class comicModel
    {
        public int Num { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public string News { get; set; }
        public string  Link { get; set; }
        public string Safe_Title { get; set; }
        public string  Transcript { get; set; }
        public string Alt { get; set; }
        public string Img { get; set; }
        public string Title { get; set; }
        public int Day { get; set; }


    //     "month": "3",
    //"num": 2276,
    //"link": "https://twitter.com/kakape/status/1235319133585248259",
    //"year": "2020",
    //"news": "",
    //"safe_title": "Self-Isolate",
    //"transcript": "",
    //"alt": "Turns out I've been \"practicing social distancing\" for years without even realizing it was a thing!",
    //"img": "https://imgs.xkcd.com/comics/self_isolate.png",
    //"title": "Self-Isolate",
    //"day": "4"
    }
}
