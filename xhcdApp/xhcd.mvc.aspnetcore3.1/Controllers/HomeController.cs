﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using xhcd.mvc.aspnetcore3._1.Models;
using RestSharp;
using System.Threading.Tasks;
using System.Collections.Generic;
using xhcd.ComicServices.Services;
using System.Net.Http;
using Newtonsoft.Json;

namespace xhcd.mvc.aspnetcore3._1.Controllers
{
    public class HomeController : Controller
    {
        private readonly IComicService _comicService;

        public HomeController(IComicService comicService)
        {
            _comicService = comicService;
        }


        public IActionResult Index()
        {
            string url = "https://xhcdapi.azurewebsites.net/comic"; //url del servicio montado en Azure;

            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);

            IRestResponse<List<comicModel>> response = client.Execute<List<comicModel>>(request);
            

            return View(response.Data);
        }

        [HttpGet]
        [Route("[controller]/Preview/{id:int}",Name ="previewDay")]
        public IActionResult Preview(int id)
        {
            int previewday = id + 1;
            string url = $"https://xhcdapi.azurewebsites.net/comic/{previewday}"; //url del servicio  en Azure;
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);

            IRestResponse<List<comicModel>> response = client.Execute<List<comicModel>>(request);
           
            return View(response.Data);
        }

        [HttpGet]
        [Route("[controller]/Next/{id:int}", Name = "nextDay")]
        public IActionResult Next(int id)
        {
            int nextDay = id + 1;
            string url = $"https://xhcdapi.azurewebsites.net/comic/{nextDay}"; //url del servicio  en Azure;
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);

            IRestResponse<List<comicModel>> response = client.Execute<List<comicModel>>(request);
           
            return View(response.Data);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

      

    }
}
