﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using xhcd.ComicServices.Services;

namespace xhcd.WebAPI.Controllers
{    
    public class ComicController : Controller
    {
        private readonly IComicService _comicService;
       
        public ComicController(IComicService comicService)
        {
            _comicService = comicService;
        }
       
        // GET: api/Comic
        [HttpGet]
        [Route("comic")]
        public async Task<IActionResult> Getall() {

            var result = await _comicService.GetComicToday();
            return Ok(result);        
        }

        // GET: api/Comic/5
        [HttpGet]
        [Route("comic/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var result = await _comicService.GetComicById(id);
            return Ok(result);

        }


    }
}
