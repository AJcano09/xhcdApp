﻿using System.Collections.Generic;
using System.Threading.Tasks;
using xhcd.ComicServices.Models;

namespace xhcd.ComicServices.Services
{
    public interface IComicService
    {
        Task<xhcdComic> GetComicToday();
        Task<xhcdComic> GetComicById(int id);
       
    }
}
