﻿using System.Collections.Generic;
using System.Threading.Tasks;
using xhcd.ComicServices.comic;
using xhcd.ComicServices.Models;

namespace xhcd.ComicServices.Services
{
    public class ComicService : IComicService
    {
        private readonly IComic _comic;

        public ComicService(IComic comic)
        {
            _comic = comic;
        }
              
        public async Task<xhcdComic> GetComicById(int id)
        {
            return await _comic.GetById(id);
        }

        public async  Task<xhcdComic> GetComicToday()
        {
           
            return await _comic.GetAll();
        }

    }
}
