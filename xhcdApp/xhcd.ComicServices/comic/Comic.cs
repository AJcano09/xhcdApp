﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using xhcd.ComicServices.Models;


namespace xhcd.ComicServices.comic
{
    public class Comic : IComic
    {
        public async Task<xhcdComic> GetAll()
        {
            //https://xkcd.com/info.0.json

            using (var client= new HttpClient())
            {
                var url = new Uri($"https://xkcd.com/info.0.json");
                var response = await client.GetAsync(url);
                string json;
                using (var content = response.Content)
                {
                    json = await content.ReadAsStringAsync();
                }
                return  JsonConvert.DeserializeObject<xhcdComic>(json);
            }
        }

        public async Task<xhcdComic> GetById(int SearchCriteria)
        {
            //https://xkcd.com/XXX/info.0.json

            using (var client = new HttpClient())
            {
                var url = new Uri($"https://xkcd.com/{SearchCriteria}/info.0.json");
                var response = await client.GetAsync(url);
                string json;
                using (var content = response.Content)
                {
                    json = await content.ReadAsStringAsync();
                }
                return JsonConvert.DeserializeObject<xhcdComic>(json);
            }
        }
    }
}
