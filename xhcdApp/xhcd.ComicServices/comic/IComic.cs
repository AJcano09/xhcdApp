﻿using System.Collections.Generic;
using System.Threading.Tasks;
using xhcd.ComicServices.Models;

namespace xhcd.ComicServices.comic
{
    public  interface IComic
    {
        Task<xhcdComic> GetAll();
        Task<xhcdComic> GetById(int SearchCriteria);
    }
}
